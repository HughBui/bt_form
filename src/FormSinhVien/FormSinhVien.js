import React, { Component } from "react";
import { connect } from "react-redux";
class FormSinhVien extends Component {
  state = {
    values: {
      maSV: "",
      hoTen: "",
      email: "",
      soDienThoai: "",
    },
    errors: {
      maSV: "",
      hoTen: "",
      email: "",
      soDienThoai: "",
    },
    valid: false,
  };
  handleChange = (e) => {
    let tagInput = e.target;
    let { name, value, type, pattern } = tagInput;

    let errorMessage = "";
    if (value.trim() === "") {
      errorMessage = name + "không được bỏ trống";
    }
    if (type === "email") {
      const regex = new RegExp(pattern);
      if (!regex.test(value)) {
        errorMessage = name + "không đúng định dạng";
      }
    }
    if (name === "soDienThoai") {
      const regex = new RegExp(pattern);
      if (!regex.test(value)) {
        errorMessage = name + "không đúng định dạng";
      }
    }
    let values = { ...this.state.values, [name]: value };
    let errors = { ...this.state.errors, [name]: errorMessage };
    this.setState(
      {
        ...this.state,
        values: values,
        errors: errors,
      },
      () => {
        console.log(this.state);
        this.checkValid();
      }
    );
  };
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.themSinhVien(this.state.values);
  };

  checkValid = () => {
    let valid = true;
    for (let key in this.state.errors) {
      if (this.state.errors[key] !== "" || this.state.values[key] === "") {
        valid = false;
      }
    }

    this.setState({
      ...this.state,
      valid: valid,
    });
  };
  render() {
    return (
      <div>
        <div className="container">
          <div className="card text-left">
            <div className="card header bg-dark text-white">
              Thông tin sinh viên
            </div>
            <div className="card-body">
              <form onSubmit={this.handleSubmit}>
                <div className="row">
                  <div className="form-group  col-6">
                    <span>Mã sinh viên</span>
                    <input
                      className="form-control"
                      name="maSV"
                      onChange={this.handleChange}
                      value={this.state.values.maSV}
                    ></input>
                    <p className="text-danger"> {this.state.errors.maSV}</p>
                  </div>
                  <div className="form-group  col-6">
                    <span>Họ tên</span>
                    <input
                      className="form-control"
                      name="hoTen"
                      onChange={this.handleChange}
                      value={this.state.values.hoTen}
                    ></input>
                    <p className="text-danger"> {this.state.errors.hoTen}</p>
                  </div>
                </div>
                <div className="row">
                  <div className="form-group  col-6">
                    <span>Email</span>
                    <input
                      type="email"
                      pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
                      className="form-control"
                      name="email"
                      onChange={this.handleChange}
                      value={this.state.values.email}
                    ></input>
                    <p className="text-danger"> {this.state.errors.email}</p>
                  </div>
                  <div className="form-group  col-6">
                    <span>Số điện thoại</span>
                    <input
                      type="number"
                      //   pattern="^\d$"
                      className="form-control"
                      name="soDienThoai"
                      onChange={this.handleChange}
                      value={this.state.values.soDienThoai}
                    ></input>
                    <p className="text-danger">
                      {this.state.errors.soDienThoai}
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12 text-right">
                    {this.state.valid ? (
                      <button type="submit" className="btn btn-success">
                        Thêm sinh viên{" "}
                      </button>
                    ) : (
                      <button
                        type="submit"
                        className="btn btn-success"
                        disabled
                      >
                        Thêm sinh viên
                      </button>
                    )}
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    themSinhVien: (sinhVien) => {
      const action = {
        type: "THEM_SINH_VIEN",
        sinhVien,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(FormSinhVien);
