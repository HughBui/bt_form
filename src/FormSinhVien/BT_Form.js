import React, { Component } from "react";
import FormSinhVien from "./FormSinhVien";
import TableSinhVien from "./TableSinhVien";

export default class BT_Form extends Component {
  render() {
    return (
      <div>
        <h3 className="container">BT Form</h3>
        <FormSinhVien />
        <TableSinhVien />
      </div>
    );
  }
}
