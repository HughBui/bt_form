import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Input, List } from "antd";

class TableSinhVien extends Component {
  renderSinhVien = () => {
    const { mangSinhVien } = this.props;
    return mangSinhVien.map((sinhVien, index) => {
      return (
        <tr key={index}>
          <td>{sinhVien.maSV}</td>
          <td>{sinhVien.hoTen}</td>
          <td>{sinhVien.soDienThoai}</td>
          <td>{sinhVien.email}</td>
          <td>
            <Button
              type="primary"
              danger
              onClick={() => {
                this.props.handleDelete(sinhVien);
              }}
            >
              Xóa
            </Button>
            <Button
              style={{ backgroundColor: "green" }}
              className=" text-white"
              onClick={() => {
                this.props.handleEdit(sinhVien);
              }}
            >
              Sửa
            </Button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <table class="table">
          <thead>
            <tr className="bg-dark text-white">
              <th>Mã sinh viên</th>
              <th>Họ Tên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>{this.renderSinhVien()}</tbody>
        </table>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    mangSinhVien: state.QuanLySinhVienReducer.mangSinhVien,
  };
};

// export default connect(mapStateToProps, null)(TableSinhVien);
export default connect(mapStateToProps, null)(TableSinhVien);
