import { combineReducers, createStore } from "redux";

const stateDefault = {
  mangSinhVien: [],
};
export const QuanLySinhVienReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case "THEM_SINH_VIEN":
      {
        const mangSVUpdate = [...state.mangSinhVien, action.sinhVien];
        state.mangSinhVien = mangSVUpdate;
        return { ...state };
      }
      break;
    default: {
      return { ...state };
    }
  }
};
export const rootReducer = combineReducers({ QuanLySinhVienReducer });
export const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
