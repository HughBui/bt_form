import logo from "./logo.svg";
import "./App.css";
import BT_Form from "./FormSinhVien/BT_Form";

function App() {
  return (
    <div className="App">
      <BT_Form />
    </div>
  );
}

export default App;
